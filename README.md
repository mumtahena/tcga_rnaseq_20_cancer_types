# README #

This repository includes code for processing RNA-Seq FASTQ files and clinical data from The Cancer Genome Atlas.

### What is this repository for? ###

* We have used the 'Rsubread' R package to align and summarize reads at the gene level for 7706 TCGA RNA-Seq tumor samples. The R scripts can also be used to process new samples. We also included the codes for compiling clinical data available for these tumors into a matrix format and matched the IDs for ease of matching phenotypes with clinical variables. Additionally, codes for analyzing the data, as described in the manuscript, are included.

### How do I get set up? ###

* Obtain access to download the raw TCGA data that you want to process.
* Process the data using our code for downstream analysis.

### Who do I talk to? ###

* Mumtahena Rahman. [moom.rahman@utah.edu](mailto:moom.rahman@utah.edu)
* Stephen R Piccolo. [https://piccolo.byu.edu](https://piccolo.byu.edu)