-----
Title: "**Pipeline for cd TCGA_20_cancers_RNA-Seq_and_clinical_variable_database_manuscript**"
Author: "Mumtahena Rahman"
date: "September 26, 2014"
last  updated date: "October 14, 2014"
output: html_document
-----
#Overall Steps:
* Download, process and normalize TCGA RNA-Seq data
    + Download tumor FASTQ files
    + Process samples with R package with 'Rsubread' to get the gene counts summerized at gene level
    + Normalize integer based counts to fragments per kilobase per million mapped reads(FPKM) and transcript per million(TPM)
* Download and process TCGA clinical data  
    + Download the clinical data
    + Match the sample IDs from the clinical data to RNA-Seq data sample IDs 
    + Create tab-delimited file that has all the clinical variables as rownames and all the samples as columns
* Analysis of data
    + Differentiating HER2 status using only the gene counts after alignment and quantification
    + Generating a HER2 signature using two pipelines and differentiating HER2 pathway status using the predictions from the HER2 signature

## 1. Download, process and normalize TCGA RNA-Seq data

### 1.a. Download, process and normalize TCGA RNA-Seq data

#### Manual steps

First, obtain an access key from the TCGA consortium that will enable you to download files from (https://cghub.ucsc.edu). Place the key in this directory and call it cghub.key. Then complete the following steps. The scripts are designed for execution on a Linux-based system.

1. Download and install the cghub software (https://cghub.ucsc.edu/software/downloads.html).
2. Download the iGenomes resource for Homo sapiens and UCSC/hg19 from (http://support.illumina.com/sequencing/sequencing_software/igenome.html). Copy iGenomes/Homo_sapiens/UCSC/hg19/Sequence/WholeGenomeFasta/genome.fa and iGenomes/Homo_sapiens/UCSC/hg19/Annotation/Genes/genes.gtf to the Genome directory.
3. Install the R statistical software (http://r-project.org) and the "Rsubread" package (http://www.bioconductor.org/packages/release/bioc/html/Rsubread.html)
4. Execute the following commands:

```
     cd Scripts
    ./identify_rnaseq_samples
    ./summarize_rnaseq
    
```


### 1.b. Normalize integer based gene counts with edgeR to get the fragments per kilobase per million mapped reads(FPKM) and Normalize FPKM values to transcript per million(TPM)

```
./normalize_rnaseq_sample

```

Using simple R code, convert FPKM values to TPM values

```
./convert_fpkm_to_tpm
```

## 2. Download and process TCGA clinical data 
#### Manual steps

Downloaded clinical data from https://tcga-data.nci.nih.gov/tcga/dataAccessMatrix.htm werbsite in 'Biotab' format building archive using "Flatten Directory Structure" preference and unzipped the downloaded folders. All the folders were renamed after specific cancer type. All the downloaded folders were then placed in one folder 

```
Rscript --vanilla Codes/ProcessClinicalData.R
```

## 3. Analysis of data

#### 3.a. Comparing gene counts

Comparing gene counts results using GFP control protein and ERBB2 protein overexpressed human mammary epithelial cell samples. TCGA's pipeline and Subread pipelines were used to generate raw gene level counts. Log based 2 gene counts are used to differentiate between control and ERBB2 overexpressed samples.

```
Rscript --vanilla Codes/AnalyzeExpression.R
```

#### 3.b. Generating HER2 signatures and comparing Her2 predictions

We predicted HER2 pathway using our overexpressed samples in human mammary epithelial cells(HMECs) in TCGA breast cancer samples. We used TCGA pipeline processed HMEC datasets on TCGA pipeline processed TCGA BRCA samples and Rsubread pipelined processed HMEC datasets on Rsubread processed TCGA BRCA samples. We only compared BRCA samples that are common and we have her2 status confirmed by immunohistochemistry. Our goal here is to compare how well these two pipeline in differenciating HER2 pathway activity where we know the status of HER2 overexpression.

### Manual steps
1. Binary regression was done to select top 200 genes for HER2 signature. Training and test samples were adjusted for batch effects using quantile normalization method.
2. The predictions were tabulated manually that were generated from FPKM log2, TPM log2 normalization methods for Rsubread processed data and TCGA RSEM normalized gene results log2 normalized data.

```
Rscript --vanilla Codes/AnalyzePredictions.R
```

Both pipelines were significant in differentiating between HER2(+) and HER(-) samples using our HER2 signature. However,  Rsubread was significantly more sensitive.
TCGA's pipeline produced predictions that has higher coeffecient of variance(0.69) compared to Rsubread's coefficient of variance(0.2-0.29).